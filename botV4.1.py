###importazione pacchetti##############################################################################
import json
import requests
import urllib
import datetime
from socket import *
from time import sleep
import datetime
import threading
#import psutil
import sys
from pyspectator.processor import Cpu
from pyspectator.computer import Computer
from pyspectator.memory import AbsMemory
from dbhelper import DBHelper
#######################################################################################################

Host ='your host IP'
Port = 2000
cpu = Cpu(monitoring_latency=1)
computer = Computer()
master = #master telegram chat id
db = DBHelper()
#cpu = psutil.cpu_times()

################################impostazioni telegram##################################################

TOKEN = "your token"
URL = "https://api.telegram.org/bot{}/".format(TOKEN)

#################################crea socket###########################################################

def Create_socket():
	s = socket(AF_INET, SOCK_STREAM)
	s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	s.bind((Host, Port))
	s.listen(5)
	connection, address = s.accept()
	print("\nconnected")
	return connection

#################################reset socket###########################################################

def Reset_socket(connection):
	connection.close()
	sleep(0.5)
	print("\nclosed")
	connection = Create_socket()
	print("\nconnecting")
	return connection

########################################################################################################

def get_url(url):
	response = requests.get(url)
	content = response.content.decode("utf8")
	#print("content:", content, "type: ",type(content))
	return content

########################################################################################################

def get_json_from_url(url):
	try:
		content = get_url(url)
		js = json.loads(content)
		return js
	except:
		print("\nFailed to load json content\n")
		#if KeyboardInterrupt:
		#	sys.exit()
		log_file = open("log_file.doc", "a")
		log_file.write("Failed to load json contetn     ")
		event_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
		log_file.write(str(event_time))
		log_file.write("\n\n")
		js = []
		return js
########################################################################################################
def get_updates(offset=None):
	url = URL + "getUpdates?timeout=1"
	if offset:
		url += "&offset={}".format(offset)
	js = get_json_from_url(url)
	return js

########################################################################################################

def get_last_update_id(updates):
	update_ids = []
	for update in updates["result"]:
		update_ids.append(int(update["update_id"]))
	return max(update_ids)

########################################################################################################

def send_message(text, chat_id, reply_markup=None):
	text = urllib.parse.quote_plus(text)
	url = URL + "SendMessage?text={}&chat_id={}&parse_mode=Markdown".format(text, chat_id)
	if reply_markup:
		url += "&reply_markup={}".format(reply_markup)
	get_url(url)

########################################################################################################

def get_text_and_chat(updates):
	len_updates = len(updates["result"])
	last_update = len_updates - 1
	try:
		text = updates["result"][last_update]["message"]["text"]
	except:
		print("no valid text")
	chat_id = updates["result"][last_update]["message"]["chat"]["id"]
	return(text, chat_id)

########################################################################################################

def write_log(name, surname, username, chat, time, ok):

	log_file = open("log_file.doc", "a")

	log_file.write(name)
	log_file.write("	")
	log_file.write(surname)
	log_file.write("	")
	log_file.write(username)
	log_file.write("	")
	log_file.write(str(chat))
	log_file.write("	")
	log_file.write(str(time))
	log_file.write("	")
	if ok == 1:
		log_file.write("Utente riconosciuto, accesso consentito")
	else:
		log_file.write("Utente NON riconosciuto, accesso NEGATO")
	log_file.write("					\n")

########################################################################################################

def write_log2(text):
	print("log2")
	time = datetime.datetime.now()
	log_file = open("temp_log.txt", "a")

	
	log_file.write(text)
	log_file.write("	")
	log_file.write(str(time))
	log_file.write("	")
	log_file.write("					\n\n\n")

#################################check ids###########################################################

def id_check(updates, allowed):
	for update in updates["result"]:
		chat = update["message"]["chat"]["id"]
		print("chat: ", chat, "allowed: ", allowed)
		date = update["message"]["date"]
		time = datetime.datetime.fromtimestamp(date)
		time = time.strftime('%Y-%m-%d at %H:%M:%S')
		try:
			name = update["message"]["chat"]["first_name"]
		except:
			#write_log2("no_name", time)
			name = "n/a"
		try:
			surname = update["message"]["chat"]["last_name"]
		except:
			#write_log2("no_surname", time)
			surname = "n/a"
		try:
			username = update["message"]["chat"]["username"]
		except:
			#write_log2("no_username", time)
			username = "n/a"
	
	if chat in allowed:
		print("\nconnection from: ", chat,"\nconnection succesfull")
		log = [name, surname, "connesso"]
		"""
		if chat != master:
			send_message(str(log), master)
		"""
		write_log(name,surname, username, str(chat), str(time), 1)
		return 1
	else:
		send_message("Utente sconosciuto, accesso negato. Contattare l'amministratore di sistema", chat)
		message = [name," ", surname,"\nUsername: ", username, "\nID: ",chat, "\nAt: ",str(time), "Concedere i privilegi all'utente?"]
		message = ''.join(map(str, message))
		keyboard = [[chat], ["Home"]]
		send_message(message, master, keyboard)
		write_log(name, surname, username, str(chat), str(time), 0)
		return 0

#################################keyboard###########################################################

def build_keyboard(n, chat):
	if n == 0:
		if chat != master:
			keyboard = [["Luci"], ["Porta"], ["Stanza"], ["Aria Condizionata"], ["Stato Server"]]
		else:
			keyboard = [["Luci", "Porta"], ["Stanza", "Aria Condizionata"], ["Stato Server", "Timer Spegnimento"], ["Impostazioni"]]
	elif n == 1:
		keyboard = [["Luci Camera"], ["Luci Comodino Camera"], ["Indietro"]]
	elif n == 2:
		keyboard = [["Accesa", "Spenta"], ["Fade", "Fade stop"], ["Rosso", "Verde"], ["Blu",  "Giallo"], ["Indietro"]]
	elif n == 3:
		keyboard = [["Umidità"], ["Temperatura"], ["Indietro"]]
	elif n == 4:
		keyboard = [["Accesa", "Spenta"], ["Programma", "Timer"], ["+1°C", "-1°C"], ["Indietro"]]
	elif n == 5:
		keyboard = [["Deumidificatore", "Aria Fredda"], ["Aria Calda", "Ventilatore"], ["Indietro"]]
	elif n == 6:
		keyboard = [["15 minuti", "30 minuti"], ["45 minuti","1 ora"], ["2 ore", "3 ore"], ["Indietro"]]
	elif n == 421 and chat == master:
		keyboard = [["Lista Utenti Con Privilegi"], ["Rimuovere Utenti"], ["Indietro"]]
		print("type_k: ", type(keyboard))
	elif n == 422 and chat == master:
		print("422")
		items = db.get_items(58677785)
		print("type_is", type(items))
		print(items)
		items.append("Indietro")
		keyboard = list()
		for item in items:
			print("\n", item)
			keyboard.append([item])
			keyboard = [[str(item)] for item in items]

	
	reply_markup = {"keyboard": keyboard, "one_time_keyboard": False}
	return json.dumps(reply_markup)

#################################keyboard choices####################################################

def keyboard_choices(b, n, text, chat, name):
	message = "Scegliere un' azione:"
	keyboard = build_keyboard(0, chat)
	ok = 1
	remove = 0
	b = 0
	if chat != master:
		ok = 0
	if text == "/start":
		n = 0
		message = "Salve! Sono HomeSweetBot, vi aiuterò con la gestione della vostra casa connessa!\nSegliete una delle seguenti opzioni per iniziare: "
	elif text == "Luci":
		n = 1
	elif text == "Luci Camera":
		n = 0
	elif text == "Luci Comodino Camera":
		n = 2
	elif text == "Accesa":
		n = 0
		message = "Luce del comodino ACCESA"
		b = 9
	elif text == "Spenta":
		n = 0
		message = "Luce del comodino SPENTA"
		b = 1
	elif text == "Fade":
		message = "Fading started"
		b = 2
	elif text == "Fade stop":
		message == "Fade stop"
		b = 3
	elif text == "Rosso":
		n = 1
		b = 4
	elif text == "Verde":
		b = 5
		n = 1
	elif text == "Blu":
		b = 6
		n = 1
	elif text == "Timer Spegnimento":
		n = 6		
		#b = 7
	elif text == "1 ora":
		n = 0
		b = 423
	elif text == "2 ore":
		n = 0
		b = 424
	elif text == "3 ore":
		n = 0
		b = 425
	elif text == "15 minuti":
		n = 0
		b = 426
	elif text == "30 minuti":
		n = 0
		b = 427
	elif text == "45 minuti":
		n = 0
		b = 428
	elif text == "Stanza":
		n = 3
	elif text == "Umidità":
		n = 0
		b = 8
	elif text == "Temperatura":
		n = 0
		b = 7
	elif text == "Stato Server":
		uptime = computer.uptime
		load = cpu.load
		temperature = cpu.temperature
		tmp = ["Uptime: ", uptime,"\nLoad: ", load, "%", "\nTemperature: ",temperature,"°C"]
		message = ''.join(map(str, tmp))
		write_log2(message)
		n = 0
	elif text == "Indietro":
		if n == 421 or n == 422:
			n = 0
		elif n == 6:
			n = 1
		elif n == 0:
			n = 0
		else:
			n = n - 1
	elif text == "Impostazioni" and chat == master:
		n = 421
	elif text == "Lista Utenti Con Privilegi" and chat == master:
		items = db.get_items(master)
		print("items: ", items)
		message = str(items)
		#write_log2(message)
		n = 421
	elif text == "Rimuovere Utenti" and chat == master:
		n = 422
		
		#b = 423
	elif text == "Home":
		n = 0		
	#elif text == "SI" and chat == master:
	print("n: ",n)
	if ok == 0:
		log = ["Connessione da: ", name, "\nID: ",chat, "\nComando: ", text]
		log = ''.join(map(str, log))
		send_message(str(log), master)
	keyboard = build_keyboard(n, chat)
	send_message(message, chat, keyboard)
	return b, n

#################################upgrade allowed####################################################

def update_allowed(allowed):
	items = db.get_items(master)
	for item in items:
		try:
			if item in allowed :
				print("no")
			else:
				#write_log2("item_missing_added", "update_allowed")
				allowed.append(int(item))
		except:
			print("\n")
	return allowed

#################################Timer####################################################

def get_name(updates):
	for update in updates["result"]:
		chat = update["message"]["chat"]["id"]
		try:
			name = update["message"]["chat"]["first_name"]
		except:
			#write_log2("no_name", time)
			name = "n/a"
		try:
			surname = update["message"]["chat"]["last_name"]
		except:
			#write_log2("no_surname", time)
			surname = "n/a"
	tmp = [name, " ", surname]
	tmp = ''.join(map(str, tmp))
	return tmp

def hello(connection):
	tmp = list()
	tmp.append(0)
	connection.send(bytearray(tmp))
	tmp = list()
	

#################################main###############################################################

def main():
	db.setup()
	connection = Create_socket()
	last_update_id = None
	a = []
	timer = 0
	b = 0
	n = 0
	allowed = [master]
	hour1 = 99
	minute1 = 0
	delta_h = 0
	delta_m = 0
	ora = 99
	minuti = 0
	#allowed = update_allowed(allowed)
	while True:
		if timer == 20:
			timer = 0
			connection = Reset_socket(connection)
		timer = timer + 1
		print("-")
		updates = get_updates(last_update_id)
		types = type(updates)
		now = datetime.datetime.now()
		hour = now.hour
		minute = now.minute
		h = []
		h.append(1)
		if hour == ora and minute == minuti:
			print("passed")
			connection.send(bytearray(h))
			if minute < 10:
				minute = str(0)+str(minute)
			tmp = ["Luci spente alle: ",hour, ":", minute]
			tmp = ''.join(map(str, tmp))
			send_message(tmp, master)
			ora = 99
			delta_m = 0
			delta_h = 0
		h = []
		#db.add_item(333333, master)
		if not updates:
			print("TypeError")
			send_message("TypeError", master)
			"""
			if '^C' or KeyboardInterrupt:
				print("Casablanca")
				send_message("System exited", master)
				sys.exit()
			"""
		else:
			if len(updates["result"]) > 0:
				last_update_id = get_last_update_id(updates) + 1
				items = db.get_items(master)
				if id_check(updates, items):
					text, chat_id = get_text_and_chat(updates)
					name = get_name(updates)
					#if text in allowed and chat_id == master:
						#db.delete_item(text, master)
					#elif text not in allowed and chat_id == master:
						#try:
							#db.add_item(text, master)
						#except:
							#print("no items")
					
					if chat_id == master:
						#print("text: ",text, "int(text): ", int(text))
						
						items = db.get_items(master)
						print("item: ", items)
						for item in items:
							print("type_i:", type(item))
						print("type:",type(text))
						try: 
							int(text)
							if isinstance(int(text), int):	
								if int(text) in items:					
									db.delete_item(int(text), master)
								else:
									db.add_item(int(text), master)
								items = db.get_items(master)
								print("item: ", items)
						except:
							pass				
						items = db.get_items(master)
					b, n = keyboard_choices(b, n, text, chat_id, name)
					print("b: ", b)
					a.append(b)	
					if b >= 423:
						print("Timer")
						print("Timer Started")
						now1 = datetime.datetime.now()
						hour1 = now1.hour
						minute1 = now1.minute
						print(hour1, minute1)
						if b == 423:
							delta_h = 1
							#print(t)
						elif b == 424:
							delta_h = 2
						elif b == 425:
							delta_h = 3
						elif b == 426:
							delta_m = 15
						elif b == 427:
							delta_m = 30
						elif b == 428:
							delta_m = 45
						if hour1 + delta_h >= 24:
							ora = hour1 + delta_h - 24
						else:
							ora = hour1 + delta_h
						if minute1 + delta_m >= 60:
							ora = hour1 + 1
							minuti = minute1 + delta_m - 60
						else:
							minuti = minute1 + delta_m
						tmp = ["Timer impostato! La luce si spegnerà alle ", ora, " : ",minuti]
						tmp = ''.join(map(str, tmp))
						send_message(tmp, chat_id)
					"""

						items = db.get_items(master)
						allowed.append(text)
						print("items: ", items, "\ntext: ", allowed,"\nstr(text): ", str(allowed[len(allowed)-1]), "\nint(text): ", int(text))
						db.delete_item(str(allowed[len(allowed)-1]), master)
						try:
							if int(text) in items:
								print("-")
						except:
							print("no delete")
						try:
							if text not in items:
								db.add_item(int(text), 58677785)

						except:
							print("no add")
						"""
					"""
						except:
							#write_log2("no _item_added_or_removed", "line_330")
							print("-ì")
						"""
						
				
					try:
						if a != 0:
							print("send1: ", a)
						connection.send(bytearray(a))
						if a[0] == 8 or a[0] == 7:
							print("U/T")
							x = list(connection.recv(2))
							sleep(0.5)
							try:
								if x[0]:
									send_message(x, chat_id)
							except:
								send_message("n/a", chat_id)
							x = list()
						sleep(1)
						a = []
					except:
						#write_log2("fail_to_send", "line_346")
						print("fail to send")
					a = []
					print("send2: ", a)	
			

if __name__ == '__main__':
	main()

